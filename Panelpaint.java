/* This project was done in a group.
 * Group Members:
 *   David Bratkov
 *   Logan Buffington
 */

import java.awt.Graphics;
import javax.swing.JPanel;

public class Panelpaint extends JPanel{
	
	box[][] boxfield = new box[8][8];
	
	public Panelpaint(){
		for(int i = 0;i < 8;i++) {
			for(int j = 0; j < 8;j++) {
				boxfield[i][j] = new box();//This double for loop assigns each box in the array with a new box
			}
		}
	}
	
	public void change(box box[][]) {
		boxfield = box;
	}
	
	 public void paint(Graphics graphic){
			int size = 7;
			for(int i=100;i < 550; i += 50){
				for(int j=100;j < 550; j += 50){
					graphic.fillOval(i, j, size, size);
				}
			}
			
			
			
			for(int i=0;i < 8;i++) { //These for loops will go through and check each side of the box and draw a line at that box
				for(int j=0;j < 8;j++) {
					
					if(boxfield[i][j].top && boxfield[i][j].bottom && boxfield[i][j].left && boxfield[i][j].right && boxfield[i][j].owner != '-') {
						graphic.drawString(boxfield[i][j].owner + "", (i*50)+125, (j*50)+125);
					}
					
					if(boxfield[i][j].top) {
						graphic.drawLine((i*50)+102, (j*50)+102, (i*50)+152, (j*50)+102);
					}
					if(boxfield[i][j].bottom) {
						graphic.drawLine((i*50)+102, (j*50)+152, (i*50)+152, (j*50)+152);
					}
					
					if(boxfield[i][j].left) {
						graphic.drawLine((i*50)+102, (j*50)+102, (i*50)+102, (j*50)+152);
					}
					
					if(boxfield[i][j].right) {
						graphic.drawLine((i*50)+152, (j*50)+102, (i*50)+152, (j*50)+152);
					}
					
				}
			}
			
			
	}
	 

}