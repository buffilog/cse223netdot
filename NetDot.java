/* This project was done in a group.
 * Group Members:
 *   David Bratkov
 *   Logan Buffington
 */

//All imports:

import java.awt.EventQueue;
import java.awt.Graphics;
import java.awt.Window;

import javax.swing.JFrame;
import javax.swing.border.EmptyBorder;
import java.awt.event.MouseEvent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.JButton;
import javax.swing.JTextField;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import javax.swing.JRadioButton;
import javax.swing.ButtonGroup;

public class NetDot extends JFrame {

	private static JPanel contentPane; //main JPanel
	private static Panelpaint gamePane; //custom JPanel with a graphic
	private JTextField Player1TextField; //place where the user stores the first player's name
	private JTextField Player2TextField; // place where the user stores the second player's name
	private JTextField AddressTextField; //place where the client can enter their desired connection address

		
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				
				try {
					
					NetDot frame = new NetDot();
					frame.setVisible(true);
					
				} catch (Exception e) {
					
					e.printStackTrace();
					
				}
				
			}
		});
	}

	//"public" variable declarations that are used throughout the file:
	//(public meaning they can be used outside of the loop they would normally be declared in)
	
	boolean playerIsHost;
	boolean turn1 = true;
	boolean validturn = false;
	static String currentPlayer = null;
	public static String player1Name = "Server";
	public static String player2Name = "Client";
	//public static int X;
	//public static int Y;
	public static boolean player1Turn = true;
	String connectAddress;
	static int score1 = 0;
	static int score2 = 0;
	private final ButtonGroup buttonGroup = new ButtonGroup();
	static box[][] boxfield;
	public static JLabel CurrentTurnLabel;
	public static JLabel Player1ScoreLabel;
	public static JLabel Player2ScoreLabel;
	
	netThread thread;
	
	public NetDot() {
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(800, 300, 437, 311);
		contentPane = new JPanel();

		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		//define buttons:
		
		JRadioButton Player1RadioButton = new JRadioButton("");
		buttonGroup.add(Player1RadioButton);
		Player1RadioButton.setBounds(80, 120, 21, 23);
		contentPane.add(Player1RadioButton);
		
		JRadioButton Player2RadioButton = new JRadioButton("");
		buttonGroup.add(Player2RadioButton);
		Player2RadioButton.setBounds(310, 120, 21, 23);
		contentPane.add(Player2RadioButton);
		
		JButton StartButton = new JButton("Start");
		StartButton.setBounds(163, 205, 89, 23);
		contentPane.add(StartButton);
		
		JButton QuitButton = new JButton("Quit Game");
		QuitButton.setBounds(50, 50, 102, 24);
		contentPane.add(QuitButton);
		
		//define text labels:
		
		CurrentTurnLabel = new JLabel("Turn");
		CurrentTurnLabel.setBounds(248, 25, 128, 26);
		contentPane.add(CurrentTurnLabel);
		
		Player1ScoreLabel = new JLabel("Player 1's score");
		Player1ScoreLabel.setBounds(400, 25, 102, 26);
		contentPane.add(Player1ScoreLabel);
		
		Player2ScoreLabel = new JLabel("Player 2's score");
		Player2ScoreLabel.setBounds(500, 25, 102, 26);
		contentPane.add(Player2ScoreLabel);
		
		JLabel Player1NameBoxLabel = new JLabel("Player 1 (Server)");
		Player1NameBoxLabel.setHorizontalAlignment(SwingConstants.CENTER);
		Player1NameBoxLabel.setBounds(50, 77, 95, 14);
		contentPane.add(Player1NameBoxLabel);
		
		JLabel Player2NameBoxLabel = new JLabel("Player 2 (Client)");
		Player2NameBoxLabel.setHorizontalAlignment(SwingConstants.CENTER);
		Player2NameBoxLabel.setBounds(275, 77, 95, 14);
		contentPane.add(Player2NameBoxLabel);
		
		JLabel StartScreenInstructionsLabel = new JLabel("Select which player you are and enter your name");
		StartScreenInstructionsLabel.setHorizontalAlignment(SwingConstants.CENTER);
		StartScreenInstructionsLabel.setVerticalTextPosition(SwingConstants.BOTTOM);
		StartScreenInstructionsLabel.setBounds(35, 11, 350, 23);
		contentPane.add(StartScreenInstructionsLabel);
		
		//define text fields:
		
		Player1TextField = new JTextField();
		Player1TextField.setBounds(52, 95, 86, 20);
		contentPane.add(Player1TextField);
		Player1TextField.setColumns(10);
		
		Player2TextField = new JTextField();
		Player2TextField.setBounds(278, 95, 86, 20);
		contentPane.add(Player2TextField);
		Player2TextField.setColumns(10);
		
		AddressTextField = new JTextField();
		AddressTextField.setBounds(105, 175, 200, 20);
		contentPane.add(AddressTextField);
		AddressTextField.setColumns(10);		
		
		//hide some of the elements not needed until gameplay starts:
		
		CurrentTurnLabel.setVisible(false);
		Player1ScoreLabel.setVisible(false);
		Player2ScoreLabel.setVisible(false);
		QuitButton.setVisible(false);
		AddressTextField.setVisible(false);
		
		//define the JPanel for the gameplay (window gets bigger = new window):
		
		gamePane = new Panelpaint();
		gamePane.setVisible(false);
		gamePane.setLayout(null);
		gamePane.setBorder(new EmptyBorder(5, 5, 5, 5));
		gamePane.setBounds(0, 0, 650, 650);
		contentPane.add(gamePane);
		
		boxfield = new box[8][8]; //This initilizes a new array of boxes
		
		//This double for loop assigns each box in the array with a new box:
		
		for(int i = 0;i < 8;i++) {
			for(int j = 0; j < 8;j++) {
				boxfield[i][j] = new box();
			}
		}
		
		gamePane.change(boxfield);
		
		//set up radio button functions:
		
		Player1TextField.setEditable(false);
		Player2TextField.setEditable(false);
		
		//If the user clicks the Player 1 Radio Button (Host):
		
		Player1RadioButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				Player1TextField.setEditable(true); //make the player 1 text field editable
				
				Player2TextField.setEditable(false); //make the player 2 text field uneditable
					Player2TextField.setText(""); //clear the player 2 text field
				
				StartButton.setText("Start"); //make the start button say "start"
				
				AddressTextField.setVisible(false); //hide the address connection prompt
				AddressTextField.setText(""); //clear the address connection prompt (makes it look nicer when it pops back up)
				
				playerIsHost = true;
				
			}
			
		});
		
		//If the user clicks the Player 2 Radio Button (Client):
		
		Player2RadioButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				Player2TextField.setEditable(true); //make the player 2 text field editable
				
				Player1TextField.setEditable(false); //make the player 1 text field uneditable
					Player1TextField.setText(""); //clear the player 1 text field
			
				StartButton.setText("Connect"); //make the start button say "connect"
				
				AddressTextField.setVisible(true); //show the address connection prompt
				
				playerIsHost = false;
				
			}
			
		});		
		
		//When the start/connect button is pressed:
		
		StartButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				//if the user has correctly filled in all information that they need to:
				
				if (Player1RadioButton.isSelected() || Player2RadioButton.isSelected()) {
					
					//hide all non-needed objects:
					
					StartScreenInstructionsLabel.setVisible(false);
					Player1TextField.setVisible(false);
					Player2TextField.setVisible(false);
					AddressTextField.setVisible(false);
					Player2NameBoxLabel.setVisible(false);
					Player1NameBoxLabel.setVisible(false);
					Player1RadioButton.setVisible(false);
					Player2RadioButton.setVisible(false);
					StartButton.setVisible(false);
					
					//set each user's name:
															
					player1Name = Player1TextField.getText();
						if(player1Name.equals("")) player1Name = "Server";
					
					player2Name = Player2TextField.getText();
						if(player2Name.equals("")) player2Name = "Client";
						
					connectAddress = AddressTextField.getText();
						if(connectAddress.equals("")) connectAddress = "localhost";

					setBounds(600, 200, 650, 650);
					//contentPane.setBounds(600, 200, 650, 650); //This is her just in case the design tab messes up the boundaries
					CurrentTurnLabel.setBounds(260, 11, 266, 37);
					
					gamePane.setVisible(true);

					CurrentTurnLabel.setVisible(true);
					Player1ScoreLabel.setVisible(true);
					Player2ScoreLabel.setVisible(true);
					QuitButton.setVisible(true);

					score1 = 0;
					score2 = 0;
					
					Player1ScoreLabel.setText(player1Name.toUpperCase().charAt(0) + ": " + score1);
					Player2ScoreLabel.setText(player2Name.toUpperCase().charAt(0) + ": " + score2);					
					
					//Open up threads for gameplay connection:
					
					if(playerIsHost) { //if the player will be hosting the server:
						
						System.out.println("You are the host and are about to open a thread");
						
						thread = new netThread("Server", player1Name);
						
					}
					else if(!playerIsHost && connectAddress.equals("localhost")) { //if the player will be connecting, but they didn't specify an IP:
						
						thread = new netThread("Client", player2Name);
						
					}
					else { //the player will be connecting and they did specify an IP:
						
						thread = new netThread("Client, player2Name", connectAddress);
						
					}
					
					thread.start(); //start the game!
					
					//gameplay:
					
					//while(running){
					
						currentPlayer = player1Name; //player 1 goes first.
					
						CurrentTurnLabel.setText(currentPlayer + "'s Turn"); //set the text box at the top to display player 1's turn
						
						//listen for mouse click:
						
						gamePane.addMouseListener(new MouseAdapter() {
							@Override
							public void mouseClicked(MouseEvent arg0) {
								
							if((player1Turn && playerIsHost) || (!player1Turn && !playerIsHost)) { //if it IS there turn:
					
								System.out.println("x=" + arg0.getX() + "y=" + arg0.getY());
								
								validturn = false; //start off the turn assuming they're gonna frick up

								validturn = Click(arg0.getX(),arg0.getY());
								
								
								
								if(validturn) {
									
									netThread.PW.println(Integer.toString(arg0.getX()) + " " + Integer.toString(arg0.getY()));
									netThread.PW.flush();
									
									player1Turn = !player1Turn;
									
									System.out.println("Valid move, info sent, and it is now the other player's turn");
									
								}
								
								if(player1Turn) currentPlayer = player1Name;
								else currentPlayer = player2Name;
								
								CurrentTurnLabel.setText(currentPlayer + "'s Turn");
								
							}
							
							else { //if it is NOT there turn:

							}
								
							//}
															
							}	
						});
						

						
						
						QuitButton.addActionListener(new ActionListener() {
							public void actionPerformed(ActionEvent arg0) {
								
								netThread.PW.println("Q");
								netThread.PW.flush();
								
								//close window and terminate program:
								
								NetDot.Quit();
								
							}
						});
											
				}
				
			}
		});		

	}
	public JTextField getTextField() {
		return Player1TextField;
	}
	
	public static boolean Click (int X, int Y) {
		
		boolean validMove = false;
		
		for(int i=0;i < 8;i++) {//This for loop will find which column of boxes the user clicked on
			
			if(((i*50+100) < X && (i*50+150) > X) || (i == 7 && X < 520)) { //this should check if its within the box
				
				for(int j=0;j < 8;j++) {//This for loop will find which row of boxes the user clicked on
					
					if((j*50+100) < Y && (j*50+150) > Y || (j == 7 && Y < 520)) {
						
						int minboxX = (i*50+100), maxboxX = (i*50+150);
						int minboxY = (j*50+100), maxboxY = (j*50+150);
						if (j == 7) maxboxY += 20;
						if (i == 7) maxboxX += 20;
						if(X < maxboxX && X > minboxX && Y < maxboxY && Y > minboxY) {//This will check if its in the area for the box
							
							int distanceFromTop = (Y%50)-2;
							int distanceFromLeft = (X%50)-2;
							int distanceFromBottom = 50 - (Y%50)-2;
							int distanceFromRight = 50 - (X%50)-2;
							
							if(X > 500) distanceFromTop = distanceFromLeft = distanceFromBottom = 100;
							if(Y > 500) distanceFromTop = distanceFromLeft = distanceFromRight = 100;
							
							/*
							System.out.println("Distance from top: " + distanceFromTop);
							System.out.println("Distance from left: " + distanceFromLeft);
							System.out.println("Distance from bottom: " + distanceFromBottom);
							System.out.println("Distance from right: " + distanceFromRight);
							*/

							if((distanceFromTop<distanceFromLeft && distanceFromTop<distanceFromRight && distanceFromTop<distanceFromBottom) && !boxfield[i][j].top) {
								boxfield[i][j].top = true;
								gamePane.change(boxfield);
								contentPane.paint(contentPane.getGraphics());
								System.out.println("top being painted!");

								if(j > 0) {
									boxfield[i][j-1].bottom = true;
								}
							
								validMove = true;
								
							}
							if((distanceFromBottom<distanceFromLeft && distanceFromBottom<distanceFromRight && distanceFromBottom<distanceFromTop) && !boxfield[i][j].bottom) {
								boxfield[i][j].bottom = true;
								gamePane.change(boxfield);
								contentPane.paint(contentPane.getGraphics());
								
								System.out.println("bottom being painted!");
								if(j < 7) {
									boxfield[i][j+1].top = true;
								}
								
								validMove = true;

							}
							if((distanceFromLeft<distanceFromTop && distanceFromLeft<distanceFromRight && distanceFromLeft<distanceFromBottom) && !boxfield[i][j].left) {
								boxfield[i][j].left = true;
								gamePane.change(boxfield);
								contentPane.paint(contentPane.getGraphics());
								
								System.out.println("left being painted!");
								if(i > 0) {
									boxfield[i-1][j].right = true;
								}
								
								validMove = true;
								
							}
							if((distanceFromRight<distanceFromLeft && distanceFromRight<distanceFromTop && distanceFromRight<distanceFromBottom) && !boxfield[i][j].right) {
								boxfield[i][j].right = true;
								gamePane.change(boxfield);
								contentPane.paint(contentPane.getGraphics());
								
								System.out.println("right being painted!");
								if(i < 7) {
									boxfield[i+1][j].left = true;
								}
								
								validMove = true;
								
							}
							
						}
					}
				}
			}
		}
		
		for(int i=0;i < 8;i++) { //These for loops will go through and check if any boxes are valid and then award points and draw a the letter of the person who won it
			for(int j=0;j < 8;j++) {
				if(boxfield[i][j].top && boxfield[i][j].bottom && boxfield[i][j].left && boxfield[i][j].right && boxfield[i][j].owner == '-') {
					
					System.out.println("Player " + NetDot.currentPlayer + " just won box " + i + " " + j);
					
					if(player1Turn){
						
						NetDot.score1++;
							
					}
					else{
						
						NetDot.score2++;
							
					}
					
					NetDot.Player1ScoreLabel.setText(player1Name.toUpperCase().charAt(0) + ": " + NetDot.score1);
					NetDot.Player2ScoreLabel.setText(player2Name.toUpperCase().charAt(0) + ": " + NetDot.score2);					 
					
					netThread.PW.println(Integer.toString(X) + " " + Integer.toString(Y));
					netThread.PW.flush();

					
					NetDot.boxfield[i][j].owner = NetDot.currentPlayer.toUpperCase().charAt(0);
					
					gamePane.change(NetDot.boxfield);
					
					contentPane.paint(contentPane.getGraphics());
					
					validMove = false;
					
				}
			}
		}
	

	return(validMove);
	
}
	
	public static void Quit() {
		
		contentPane.setVisible(false);
		System.exit(0);
		
	}
	
}