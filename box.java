/* This project was done in a group.
 * Group Members:
 *   David Bratkov
 *   Logan Buffington
 */

public class box {
	boolean top; //These booleans for each side of the box will draw a line if true and no line if false
	boolean bottom;
	boolean left;
	boolean right;
	char owner; //This char is used to store the initial of the owner that won that box
	
	public box() {//this constructor sets all the default values for when this class gets called
		
		top = false; 
		bottom = false;
		left = false;
		right = false;
		owner = '-'; 
		
	}
	
}