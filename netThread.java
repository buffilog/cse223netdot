/* This project was done in a group.
 * Group Members:
 *   David Bratkov
 *   Logan Buffington
 */

import java.io.IOException;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Scanner;

public class netThread extends Thread{
	
	ServerSocket SS;
	Socket S;
	public Scanner SC;
	public static PrintWriter PW;
	
	String Input;
	String IP = "localhost";
	String Name;
	int X,Y;
	
	public netThread(String input,String name) {
		Name = name; //figure out how to pass the name and how to sent it to server and clients
		Input = input;
	}
	
	public netThread(String input,String name, String ip) {
		Name = name;
		IP = ip;
		Input = input;
	}
	
	public void run(){
		
		if(Input.contains("Server")) {
			
			try {//This is the initialization for the server 
				SS = new ServerSocket(1234);
				System.out.println("Opened new serversocket instance");
				
			}
			
			catch (IOException e) {
				e.printStackTrace();
				return;
			}
			
			try { //This accepts the first connection to the server
				S = SS.accept();
				System.out.println("Just ran SS.accept and made a socket for that");
				
			}
			
			catch (IOException e) {
				e.printStackTrace();
				return;
			}
			
		}
		
		if(Input.contains("Client")) {
			
			try {
				
				S = new Socket(IP,1234);
				
				System.out.println("New Client Socket made @ 1234");
				
			} catch (UnknownHostException e) {
				
				e.printStackTrace();
				
			} catch (IOException e) {
				
				e.printStackTrace();
				
				//PW.println() // figure out how to send name to server
				
			}
			
		}
		
		try { 
			SC = new Scanner(S.getInputStream());
			PW = new PrintWriter(S.getOutputStream());
		}
		
		catch (IOException e) {
			
			e.printStackTrace();
			return;
			
		}
		
		System.out.println("thread succesfully opened");
		
		PW.println(Name); //once thread is opened, send the other user your name
		PW.flush(); //flush 
		
		System.out.println("Your name has been sent");
		
		boolean nameAccepted = false;
			
			while(SC.hasNextLine()) {
				
				String temp = SC.nextLine();
				
				System.out.println("Line has been accepted from other user");
				
				if(!nameAccepted) { //if the line from SC is the other players name (first input from other user will be their name):
					
					if(Input.equals("Server")) { //and they are the server host (player 1):
						
						NetDot.player2Name = temp; //set player 2's name (the other player) to the line you just got.
						
					}
					else { //else if they are the client (player 2):
						
						NetDot.player1Name = temp; //set player 1's name (the other player) to the line you just got.
						
					}
					
					
					System.out.println("Other user's name has been set");
					
				}
				
				if(temp.equals("Q")) {
					
					System.out.println("Quitting!");
					
					NetDot.Quit();
					
					try {
						SC.close();
						SS.close();
						S.close();
					}
					catch (Exception e) {
						
					}
					
				}
				
				if(nameAccepted) {
					
					System.out.println("recived coords! raw string:" + temp);

					String[] splitCoords = temp.split(" "); //split it up by spaces (our custom format)
			
					boolean turn = false;


					X = Integer.parseInt(splitCoords[0]); //set the x coordinate of the move to the first part of the string
					
					Y = Integer.parseInt(splitCoords[1]); //set the y coordinate of the move to the second part of the string	
					
					turn = NetDot.Click(X, Y);

					if(turn) {
						
						NetDot.player1Turn = !NetDot.player1Turn;
						
						if(NetDot.player1Turn) NetDot.currentPlayer = NetDot.player1Name;
						else NetDot.currentPlayer = NetDot.player2Name;
						
						if(NetDot.player1Turn) NetDot.CurrentTurnLabel.setText(NetDot.player1Name + "'s Turn");
						else NetDot.CurrentTurnLabel.setText(NetDot.player2Name + "'s Turn");
						
					}
					
					
					
				}
				
				nameAccepted = true; //set flag that you have recieved their name (next line will now be gameplay-based).

			}
	
	}
	
}